<!DOCTYPE html>
<html lang="en">

<head>
    <title>Employee Login</title>
    <?php include_once('./layouts/links.php'); ?>
</head>

<body>
    <?php 
    include_once('./layouts/navigation.php'); 
    
    if(array_key_exists('ses_epf', $_SESSION)){
        header("location:./order_meals.php");
    } else if(array_key_exists('ses_username', $_SESSION)){
        header("location:./admin_employee.php");
    }
 
    $epf = $epf_e = $psw = $psw_e = "" ;
    if(array_key_exists("emp_login", $_POST)){
        $epf = $_POST['epf'];
        $psw = $_POST['psw'];
        $valid = true;
        if(empty($epf)){
            $epf_e = "Enter EFP Number to Login";
            $valid = false;
        }
        if(empty($psw)){
            $psw_e = "Enter Password to Login";
            $valid = false;
        }
        if($valid){
            $psw = md5($psw);
            $sql = "SELECT * FROM users WHERE epf_no='$epf'";
            $result = $__conn->query($sql);
            if ($result->num_rows == 1) {
                $sql = "SELECT * FROM users WHERE epf_no='$epf' AND password='$psw'";
                $result = $__conn->query($sql);
                if ($result->num_rows == 1) {
                    $row = $result->fetch_assoc();
                    $_SESSION['ses_epf'] = $row['epf_no'];
                    header("location:order_meals.php");
                } else {
                    $psw_e = "Invalid Password";
                }
            } else {
                $epf_e = "Invalid EPF Number";
            }

        }
        
    }
    ?>

    <div class="row content">
        <div class="col-12 col-md-6 d-flex align-items-center justify-content-center">
            <div><img src="./img/image_1.png" class="img-fluid main-icon"></div>
        </div>
        <div class="col-12 col-md-6">
            <div class="box h-100 border-1 d-flex align-items-center">
                <div class="w-100">
                    <div class="title-1 mb-4">Employee Login</div>
                    <div class="box bg-1 d-flex align-items-center mb-5">
                        <div><img src="./img/brand.png" class="icon-small"></div>
                        <div class="sep ms-4">|</div>
                        <div class="desc-1 ms-4">Welcome To Meal Order System</div>
                    </div>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                        <div class="mb-3">
                            <label for="epf" class="form-label mb-0">Employee EPF <span
                                    class="red"><?php echo $epf_e; ?></span></label>
                            <input type="text" class="form-control" id="epf" name="epf" autocomplete="new-password"
                                value="<?php echo $epf; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="psw" class="form-label mb-0">Password <span
                                    class="red"><?php echo $psw_e; ?></span></label>
                            <input type="password" class="form-control" id="psw" name="psw" autocomplete="new-password">
                        </div>
                        <div class="form-check mb-5">
                            <input class="form-check-input" type="checkbox" id="psw-v">
                            <label class="form-check-label" for="psw-v">
                                Show Password
                            </label>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-pri" name="emp_login">Log in</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>