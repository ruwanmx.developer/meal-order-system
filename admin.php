<!DOCTYPE html>
<html lang="en">

<head>
    <title>Admin Login</title>
    <?php include_once('./layouts/links.php'); ?>
</head>

<body>
    <?php 
    include_once('./layouts/navigation.php'); 
    
    $username = $username_e = $psw = $psw_e = "" ;
    if(array_key_exists("admin_login", $_POST)){
        $username = $_POST['username'];
        $psw = $_POST['psw'];
        $valid = true;
        if(empty($username)){
            $username_e = "Enter EFP Number to Login";
            $valid = false;
        }
        if(empty($psw)){
            $psw_e = "Enter Password to Login";
            $valid = false;
        }
        if($valid){
            $psw = md5($psw);
            $sql = "SELECT * FROM system_users WHERE username='$username'";
            $result = $__conn->query($sql);
            if ($result->num_rows == 1) {
                $sql = "SELECT * FROM system_users WHERE username='$username' AND password='$psw'";
                $result = $__conn->query($sql);
                if ($result->num_rows == 1) {
                    $row = $result->fetch_assoc();
                    $_SESSION['ses_username'] = $row['username'];
                    header("location:admin_employee.php");
                } else {
                    $psw_e = "Invalid Password";
                }
            } else {
                $username_e = "Invalid Username";
            }

        }
        
    }

    ?>

    <div class="row content">
        <div class="col-12 col-md-6 d-flex align-items-center justify-content-center">
            <div><img src="./img/image_2.png" class="img-fluid  main-icon"></div>
        </div>
        <div class="col-12 col-md-6">
            <div class="box h-100 border-1 d-flex align-items-center">
                <div class="w-100">
                    <div class="title-1 mb-4">Admin Login</div>
                    <div class="box bg-1 d-flex align-items-center mb-5">
                        <div class="d-flex"></div>
                        <div><img src="./img/brand.png" class="icon-small"></div>
                        <div class="sep ms-4">|</div>
                        <div class="desc-1 ms-4">Welcome To Meal Order System</div>
                    </div>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                        <div class="mb-3">
                            <label for="username" class="form-label mb-0">Username <span
                                    class="red"><?php echo $username_e; ?></span></label>
                            <input type="text" class="form-control" id="username" name="username"
                                autocomplete="new-password" value="<?php echo $username; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label mb-0">Password <span
                                    class="red"><?php echo $psw_e; ?></span></label>
                            <input type="password" class="form-control" id="password" name="psw"
                                autocomplete="new-password">
                        </div>
                        <div class="form-check mb-5">
                            <input class="form-check-input" type="checkbox" id="psw-v">
                            <label class="form-check-label" for="psw-v">
                                Show Password
                            </label>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-pri" name="admin_login">Log in</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>