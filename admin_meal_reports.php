<!DOCTYPE html>
<html lang="en">

<head>
    <title>Meal Reports</title>
    <?php include_once('./layouts/links.php'); ?>
</head>

<body>
    <?php 
    include_once('./layouts/navigation.php'); 

    if(!array_key_exists('ses_username', $_SESSION)){
        header("location:./index.php");
    }

    $date = $epf = $dep = $meal = "";
    if(array_key_exists("filter", $_POST)){

        if(array_key_exists("date", $_POST)){
            $date = $_POST['date'];
        }
        if(array_key_exists("epf", $_POST)){
            $epf = $_POST['epf'];
        }
        if(array_key_exists("dep", $_POST)){
            $dep = $_POST['dep'];
        }
        if(array_key_exists("meal", $_POST)){
            $meal = $_POST['meal'];
        }
       
    }

    $page1 = $page2 = $page3 = "";
    $page2 = "active";
    ?>

    <div class="row content">
        <?php include_once('./layouts/admin_menu.php'); ?>
        <div class="col-12 col-md-8 col-lg-9 col-xl-10 h-100">
            <div class="row">
                <div class="col-12 h-100">
                    <div class="title-1 mb-4">Meal Reports</div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                            <div class="row gy-3">
                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Date</label>
                                    <input type="text" name="date" id="" class="form-control">
                                </div>
                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Employee EPF</label>
                                    <input type="text" name="epf" id="" class="form-control">
                                </div>
                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Department</label>
                                    <select name="dep" class="form-select" aria-label="Default select example">
                                        <option value="">All</option>
                                        <?php 
                                        $sql = "SELECT * FROM departments";
                                        $result = $__conn->query($sql);
                                        while($row = $result->fetch_assoc()) {
                                            $selected = "";
                                            if($dep == $row['id']){
                                                $selected = "selected";
                                            }
                                            echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['department'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Meal Type</label>
                                    <select name="meal" class="form-select" aria-label="Default select example">
                                        <option value="">All</option>
                                        <option <?php if($meal === "Chicken"){echo "selected" ;}?> value="Chicken">
                                            Chicken</option>
                                        <option <?php if($meal === "Egg"){echo "selected" ;}?> value="Egg">Egg</option>
                                        <option <?php if($meal === "Fish"){echo "selected" ;}?> value="Fish">Fish
                                        </option>
                                        <option <?php if($meal === "Vegitable"){echo "selected" ;}?> value="Vegitable">
                                            Vegitable</option>
                                    </select>
                                </div>
                                <div class="col-8 d-flex admin-form justify-content-end">
                                    <button name="filter" type="submit" class="btn btn-pri">Filter</button>
                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
            <div class="row marg-t-3">
                <div class="col-12">
                    <div class="box admin-employee-wrap">
                        <div class="admin-employee-list-wrap">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" class="title-3">Employee EPF</th>
                                        <th scope="col" class="title-3">Employee Name</th>
                                        <th scope="col" class="title-3">Department</th>
                                        <th scope="col" class="title-3">Meal Type</th>
                                        <th scope="col" class="title-3">Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sql = "SELECT a.epf_no, a.name, b.department, c.meal_type, c.date FROM users a INNER JOIN departments b ON a.department = b.id INNER JOIN meal_orders c ON c.epf_no = a.epf_no WHERE c.epf_no LIKE '%$epf%' AND c.meal_type LIKE '%$meal%' AND c.date LIKE '%$date%' AND a.department LIKE '%$dep%'";
                                    $result = $__conn->query($sql);
                                    while($row = $result->fetch_assoc()) {
                                        echo '<tr>
                                        <td>'.$row['epf_no'].'</td>
                                        <td>'.$row['name'].'</td>
                                        <td>'.$row['department'].'</td>
                                        <td>'.$row['meal_type'].'</td>
                                        <td>'.$row['date'].'</td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>