<!DOCTYPE html>
<html lang="en">

<head>
    <title>My Profile</title>
    <?php include_once('./layouts/links.php'); ?>
</head>

<body>
    <?php 
    include_once('./layouts/navigation.php'); 
    
    if(!array_key_exists('ses_epf', $_SESSION)){
        header("location:./index.php");
    }

    $epf = $_SESSION['ses_epf'];

    $sql = "SELECT a.epf_no, a.name, b.department FROM users a INNER JOIN departments b ON a.department = b.id WHERE epf_no='$epf'";
    $result = $__conn->query($sql);
    $row = $result->fetch_assoc();

    $page1 = $page2 = $page3 = "";
    $page3 = "active";
    ?>

    <div class="row content">
        <?php include_once('./layouts/employee_menu.php'); ?>
        <div class="col-12 col-md-8 col-lg-9 col-xl-10 h-100">
            <div class="row">
                <div class="col-12 h-100">
                    <div class="title-1 mb-4">My Profile</div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="box meal-report-card">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th style="max-width:30px;">Employee Name</td>
                                    <td><?php echo $row['name']; ?></td>
                                </tr>
                                <tr>
                                    <th style="max-width:30px;">Employee EPF</td>
                                    <td><?php echo $row['epf_no']; ?></td>
                                </tr>
                                <tr>
                                    <th style="max-width:30px;">Department</td>
                                    <td><?php echo $row['department']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>