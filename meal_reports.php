<!DOCTYPE html>
<html lang="en">

<head>
    <title>Meal Reports</title>
    <?php include_once('./layouts/links.php'); ?>
</head>

<body>
    <?php 
    include_once('./layouts/navigation.php'); 

    if(!array_key_exists('ses_epf', $_SESSION)){
        header("location:./index.php");
    }

    $epf = $_SESSION['ses_epf'];

    $sql = "SELECT * FROM meal_orders WHERE epf_no='$epf'";
    $result = $__conn->query($sql);

    $page1 = $page2 = $page3 = "";
    $page2 = "active";
    
    ?>

    <div class="row content">
        <?php include_once('./layouts/employee_menu.php'); ?>
        <div class="col-12 col-md-8 col-lg-9 col-xl-10 h-100">
            <div class="row">
                <div class="col-12 h-100">
                    <div class="title-1 mb-4">Meal Reports</div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="box meal-report-card">
                        <div class="meal-report-list-wrap">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" class="title-3">Meal Type</th>
                                        <th scope="col" class="title-3">Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while($row = $result->fetch_assoc()) {
                                        echo "<tr>
                                        <td>".$row['meal_type']."</td>
                                        <td>".$row['date']."</td>
                                        </tr>";
                                    }
                                    ?>

                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>

</html>