<!DOCTYPE html>
<html lang="en">

<head>
    <title>Order Meal</title>
    <?php include_once('./layouts/links.php'); ?>
</head>

<body>
    <?php 
    include_once('./layouts/navigation.php'); 
    
    if(!array_key_exists('ses_epf', $_SESSION)){
        header("location:./index.php");
    }

    $sql = "SELECT  DATE_FORMAT(CURDATE(),'%Y-%m-%d') AS today";
    $result = $__conn->query($sql);
    $row = $result->fetch_assoc();

    $today = $row['today'];
    $epf = $_SESSION['ses_epf'];

    if(array_key_exists("meal_order", $_POST)){
        $meal = $_POST['meal_type'];

        $sql = "INSERT INTO meal_orders VALUES(NULL, '$epf', '$meal', CAST('". $today ."' AS DATE))";
        $__conn->query($sql);
    }

    $sql = "SELECT * FROM meal_orders WHERE epf_no='$epf' AND date='$today'";
    $result = $__conn->query($sql);
    $order_status = false;
    $order = "";
    if ($result->num_rows == 1) {
        $order_status = true;
        $row = $result->fetch_assoc();
        $order_type = $row['meal_type'];
    }

    $page1 = $page2 = $page3 = "";
    $page1 = "active";
    ?>
    <div class="row content">
        <?php include_once('./layouts/employee_menu.php'); ?>
        <div class="col-12 col-md-8 col-lg-9 col-xl-10 h-100">
            <div class="row">
                <div class="col-12 h-100">
                    <div class="title-1 mb-4">Order Meals</div>
                    <div class="box marg-b">Meal Date <?php echo $today; ?></div>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="box menu-card border-1 text-center">
                        <div class="title-2">Chicken</div>
                        <div>
                            <img src="./img/chic.png" class="icon-big">
                        </div>
                        <div class="desc-1 mb-5">
                            Rice<br>
                            Chicken Curry<br>
                            Dhal Curry<br>
                            Cabbage Curry<br>
                            Spicy Potato Curry
                        </div>
                        <?php if($order_status === false ){?>
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                            <input type="hidden" value="Chicken" name="meal_type">
                            <button type="submit" name="meal_order" class="btn btn-pri order-btn">Order</button>
                        </form>
                        <?php } else if($order_type === 'Chicken' ){?>
                        <button name="meal_order" class="btn btn-pri order-btn">Ordered</button>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-3">
                    <div class="box menu-card border-1 text-center">
                        <div class="title-2">Fish</div>
                        <div>
                            <img src="./img/fish.png" class="icon-big">
                        </div>
                        <div class="desc-1 mb-5">
                            Rice<br>
                            Fish Curry<br>
                            Dhal Curry<br>
                            Cabbage Curry<br>
                            Spicy Potato Curry
                        </div>
                        <?php if($order_status === false ){?>
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                            <input type="hidden" value="Fish" name="meal_type">
                            <button type="submit" name="meal_order" class="btn btn-pri order-btn">Order</button>
                        </form>
                        <?php } else if($order_type === 'Fish' ){?>
                        <button name="meal_order" class="btn btn-pri order-btn">Ordered</button>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-3">
                    <div class="box menu-card border-1 text-center">
                        <div class="title-2">Egg</div>
                        <div>
                            <img src="./img/egg.png" class="icon-big">
                        </div>
                        <div class="desc-1 mb-5">
                            Rice<br>
                            Boiled Egg<br>
                            Dhal Curry<br>
                            Cabbage Curry<br>
                            Spicy Potato Curry
                        </div>
                        <?php if($order_status === false ){?>
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                            <input type="hidden" value="Egg" name="meal_type">
                            <button type="submit" name="meal_order" class="btn btn-pri order-btn">Order</button>
                        </form>
                        <?php } else if($order_type === 'Egg' ){?>
                        <button name="meal_order" class="btn btn-pri order-btn">Ordered</button>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-3">
                    <div class="box menu-card border-1 text-center">
                        <div class="title-2">Vegitable</div>
                        <div>
                            <img src="./img/vegi.png" class="icon-big">
                        </div>
                        <div class="desc-1 mb-5">
                            Rice<br>
                            Soya Meat Curry<br>
                            Dhal Curry<br>
                            Cabbage Curry<br>
                            Spicy Potato Curry
                        </div>
                        <?php if($order_status === false ){?>
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                            <input type="hidden" value="Vegitable" name="meal_type">
                            <button type="submit" name="meal_order" class="btn btn-pri order-btn">Order</button>
                        </form>
                        <?php } else if($order_type === 'Vegitable' ){?>
                        <button name="meal_order" class="btn btn-pri order-btn">Ordered</button>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>