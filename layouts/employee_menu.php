<div class="col-12 col-md-4 col-lg-3 col-xl-2">
    <div class="box h-100">
        <a href="./order_meals.php" class="no-link <?php echo $page1; ?>">
            <div class="nav-t text-center ms-0">Order Meals</div>
        </a>
        <div class="sep-h"></div>
        <a href="./meal_reports.php" class="no-link <?php echo $page2; ?>">
            <div class="nav-t text-center ms-0">Meal Reports</div>
        </a>
        <div class="sep-h"></div>
        <a href="./my_profile.php" class="no-link <?php echo $page3; ?>">
            <div class="nav-t text-center ms-0">My Profile</div>
        </a>
        <div class="sep-h"></div>
        <a href="./database/logout.php" class="no-link">
            <div class="nav-t text-center ms-0">Logout</div>
        </a>
    </div>
</div>