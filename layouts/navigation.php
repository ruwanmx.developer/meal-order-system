<?php require_once('./database/config.php'); ?>
<div class="row marg-b">
    <div class="col-12">
        <div class="box">
            <div class="row">
                <div class="col-10 d-flex align-items-center">
                    <div class="brand-set">
                        <img src="./img/brand.png" alt="">
                        <div class="ms-2"><span>Meal Order</span><br>System</div>
                    </div>
                    <div class="nav-t d-none d-md-block">Special Notice</div>
                    <div class="nav-t d-none d-md-block">Contact Us</div>
                </div>
                <div class="col-2 d-flex justify-content-end align-items-center">
                    <?php 
                    if(array_key_exists('ses_epf', $_SESSION)){
                        echo '<a href="./my_profile.php"><div><button class="btn btn-pri">My Profile</button></div></a>';
                    } else if(array_key_exists('ses_username', $_SESSION)){
                        echo '';
                    } else {
                        echo '<a href="./index.php"><div><button class="btn btn-pri">Login</button></div></a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>