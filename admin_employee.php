<!DOCTYPE html>
<html lang="en">

<head>
    <title>Manage Employee</title>
    <?php include_once('./layouts/links.php'); ?>
</head>

<body>
    <?php 
    include_once('./layouts/navigation.php'); 
    
    if(!array_key_exists('ses_username', $_SESSION)){
        header("location:./index.php");
    }

    $name = $epf = $department = $psw = $err ="";
    if(array_key_exists("add_emp", $_POST)){
        $name = $_POST['name'];
        $epf = $_POST['epf'];
        $department = $_POST['department'];
        $psw = $_POST['psw'];
        $valid = true;

        if(empty($psw)){
            $err = "Enter Employee Password";
            $valid = false;
        }
        if(empty($epf)){
            $err = "Enter Employee EPF";
            $valid = false;
        }
        if(empty($name)){
            $err = "Enter Employee Name";
            $valid = false;
        }
        if($valid){
            $sql = "SELECT * FROM users WHERE epf_no='$epf'";
            $result = $__conn->query($sql);
            if ($result->num_rows == 1) {
                $err = "Employee EPF already registerd";
            } else {
                $psw = md5($psw);
                $sql = "INSERT INTO users VALUES(NULL, '$name', '$epf', '$department', '$psw')";
                $__conn->query($sql);
                $name = $epf = $psw = $err ="";
                $department = 1;
            }
            
        }
    }

    $page1 = $page2 = $page3 = "";
    $page1 = "active";
    ?>

    <div class="row content">
        <?php include_once('./layouts/admin_menu.php'); ?>
        <div class="col-12 col-md-8 col-lg-9 col-xl-10 h-100">
            <div class="row">
                <div class="col-12 h-100">
                    <div class="title-1 mb-4">Employees</div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                            <div class="row gy-3">

                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Empolyee Name : </label>
                                    <input type="text" name="name" id="" class="form-control"
                                        value="<?php echo $name; ?>">
                                </div>
                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Employee EPF : </label>
                                    <input type="text" name="epf" id="" class="form-control"
                                        value="<?php echo $epf; ?>">
                                </div>
                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Department : </label>

                                    <select name="department" class="form-select" aria-label="Default select example">
                                        <?php 
                                        $sql = "SELECT * FROM departments";
                                        $result = $__conn->query($sql);
                                        while($row = $result->fetch_assoc()) {
                                            $selected = "";
                                            if($department == $row['id']){
                                                $selected = "selected";
                                            }
                                            echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['department'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-4 d-flex admin-form">
                                    <label for="" class="adm">Password : </label>
                                    <input type="text" name="psw" id="" class="form-control">
                                </div>
                                <div class="col-4 d-flex admin-form justify-content-center">
                                    <label for="" class="adm"><span class="red"><?php echo $err; ?></span></label>
                                </div>
                                <div class="col-4 d-flex admin-form justify-content-end">
                                    <button name="add_emp" type="submit" class="btn btn-pri">Add Employee</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row marg-t-3">
                <div class="col-12">
                    <div class="box admin-employee-wrap">
                        <div class="admin-employee-list-wrap">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" class="title-3">Employee EPF</th>
                                        <th scope="col" class="title-3">Employee Name</th>
                                        <th scope="col" class="title-3">Department</th>
                                        <th scope="col" class="title-3"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sql = "SELECT a.epf_no, a.name, b.department FROM users a INNER JOIN departments b ON a.department = b.id";
                                    $result = $__conn->query($sql);
                                    while($row = $result->fetch_assoc()) {
                                        echo '<tr>
                                        <td>'.$row['epf_no'].'</td>
                                        <td>'.$row['name'].'</td>
                                        <td>'.$row['department'].'</td>
                                        <td style="text-align:right;"><a href="./database/remove_user.php?epf='.$row['epf_no'].'"><div><button class="btn btn-pri">Remove</button></div></a></td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>